type !'a mon = 'a
type !'a t = 'a
val return : 'a -> 'a t
val bind : 'a t -> ('a -> 'b t) -> 'b t
