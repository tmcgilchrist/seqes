(**************************************************************************)
(*                                                                        *)
(*                                 OCaml                                  *)
(*                                                                        *)
(*                 Simon Cruanes                                          *)
(*                                                                        *)
(*   Copyright 2017 Institut National de Recherche en Informatique et     *)
(*     en Automatique.                                                    *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

(** Monadic traversors over {!Stdlib.Seq}.

    This [Make] functor produces functions to traverse {!Stdlib.Seq}
    sequences, when the function provided by the user is within a monad.

    The [Make] functor does not export a new type: it only exports
    traversors for the existing type exported by the Stdlib. If you need tighter
    integration between your monad and the sequence, checkout
    {!Seqes.Monadic}.

    The buld of the documentation is located in the {!Seqes.Sigs1} module.
    Familiarity with the {!Stdlib.Seq} module is also assumed.
 *)
module Make1(Mon: Sigs1.MONAD1)
: Sigs1.SEQMON1TRAVERSORS
    with type 'a mon := 'a Mon.t
    with type 'a callermon := 'a Mon.t
    with type 'a t := 'a Stdlib.Seq.t

module Make2(Mon: Sigs2.MONAD2)
: Sigs2.SEQMON2TRAVERSORS
    with type ('a, 'e) mon := ('a, 'e) Mon.t
    with type ('a, 'e) callermon := ('a, 'e) Mon.t
    with type ('a, 'e) t := 'a Stdlib.Seq.t
