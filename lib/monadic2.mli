(**************************************************************************)
(*                                                                        *)
(*                                 OCaml                                  *)
(*                                                                        *)
(*                 Simon Cruanes                                          *)
(*                                                                        *)
(*   Copyright 2017 Institut National de Recherche en Informatique et     *)
(*     en Automatique.                                                    *)
(*                                                                        *)
(*                 Raphaël Proust                                         *)
(*                                                                        *)
(*   Copyright 2022 Nomadic Labs                                          *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

(** Monad functors over Stdlib Sequences

   {!Stdlib.Seq} provides an abstraction for delayed lists. One limitation of
   the {!Stdlib.Seq} module is that its type cannot accommodate monads. For
   example, considering a cooperative I/O monad in the style of Async or Lwt,
   using the type [('a, 'e) io] to denote promises of [('a, 'e) result], one
   cannot write
   {[
     val map_io : ('a -> ('b, 'e) io) -> 'a Seq.t -> 'b Seq.t
   ]}

   That's because the [Seq.t] type includes an arrow ([->]) that is outside of
   the monad, as per the type declaration.

   This here module exposes a [Make2] functor to produce new [Seq]-like
   modules with a baked-in 2-type-parameters monad.

   Recommended readings:

   - Familiarity with {!Stdlib.Seq} is assumed.
   - See {!Sigs2} for an explanation of the separation of Traversors,
   Transformers, and the rest.
   - See {!Monadic1} for the 1-parameter monad variant of this module.

   *)

(** The module type for the output of the {!Make2} module (below). *)
module type S2 = sig

  (** The type of the monad that the specialised Seq variation is built upon.
      This type is destructively substituted by the application of the functor;
      it is replaced by the actual monad type passed as parameter. *)
  type ('a, 'e) mon

  (** The Seq-like type: identical to [Stdlib.Seq.t] except for [mon]. *)
  type ('a, 'e) t = unit -> (('a, 'e) node, 'e) mon
  and ('a, 'e) node =
    | Nil
    | Cons of 'a * ('a, 'e) t

  (** This [include] brings all the functions from the [Stdlib.Seq] module but
      specialised to the specialised Seq variation. E.g., given
      [module SeqMon = Make2(Mon)] then the function [SeqMon.map] has type
      [('a -> 'b) -> ('a, 'e) t -> ('b, 'e) t] and [SeqMon.iter] has type
      [('a -> unit) -> ('a, 'e) t -> (unit, 'e) mon].

      See the documentation of {!Sigs2.SEQMON2ALL} for more details. *)
  include Sigs2.SEQMON2ALL
    with type ('a, 'e) mon := ('a, 'e) mon
    with type ('a, 'e) t := ('a, 'e) t

  (** [M] is a module which contains a specialised subset of the functions from
      the [Stdlib.Seq] module. Specifically, it contains those functions which
      take a function as parameter (e.g., [map] but not [length]). Moreover,
      those parameter functions' return type is specialised to be within the
      [mon] monad. E.g., given [module SeqMon = Make2(Mon)] then [SeqMon.M.map]
      has type [('a -> ('b, 'e) Mon.t) -> ('a, 'e) t -> ('b, 'e) t] and
      [SeqMon.M.iter] has type
      [('a -> (unit, 'e) mon) -> ('a, 'e) t -> (unit, 'e) mon].

      See the documentation of {!Sigs2.SEQMON2TRANSFORMERS} for more details. *)
  module M
  : Sigs2.SEQMON2TRANSFORMERS
    with type ('a, 'e) mon := ('a, 'e) mon
    with type ('a, 'e) callermon := ('a, 'e) mon
    with type ('a, 'e) t := ('a, 'e) t

  (** [Make] is a functor to produce further [M]-like modules, but with
      parameter functions returning into a different monad. E.g., given
      [module SeqMon = Make(Mon)] and
      [module SeqMonMun = SeqMon.Make(Mun)] then [SeqMonMun.map] has type
      [('a -> ('b, 'e) Mun.t) -> ('a, 'e) t -> ('b, 'e) t].

      Note that the functor parameter includes the necessary machinery to bundle
      the two monads together.

      See the documentation of {!Sigs2.SEQMON2TRANSFORMERS} for more details. *)
  module Make
    (Alt : Sigs2.MONAD2)
    (Glue : Sigs2.GLUE2
      with type ('a, 'e) x := ('a, 'e) Alt.t
      with type ('a, 'e) f := ('a, 'e) mon
      with type ('a, 'e) ret := ('a, 'e) mon)
  : Sigs2.SEQMON2TRANSFORMERS
      with type ('a, 'e) mon := ('a, 'e) mon
      with type ('a, 'e) callermon := ('a, 'e) Alt.t
      with type ('a, 'e) t := ('a, 'e) t

  (** [MakeTraversors] is a functor similar to [Make]. It produces only a subset
      of the functions that [Make] does. Specifically, it produces the subset of
      functions that traverse a sequence (or part thereof) and return a value
      which is not a sequence (e.g., [iter] returning [unit] but not [map]
      returning a new sequence).

      In this subset it is possible to mix the monad in more complex ways. And
      thus the monad-combining machinery provided as functor parameter is more
      complex.

      See [examples/] for use.

      See the documentation of {!Sigs2.SEQMON2TRAVERSORS} for more details. *)
  module MakeTraversors
    (Alt : Sigs2.MONAD2)
    (Ret : Sigs2.MONAD2)
    (GlueAlt : Sigs2.GLUE2
      with type ('a, 'e) x := ('a, 'e) Alt.t
      with type ('a, 'e) f := ('a, 'e) Ret.t
      with type ('a, 'e) ret := ('a, 'e) Ret.t)
    (GlueMon : Sigs2.GLUE2
      with type ('a, 'e) x := ('a, 'e) mon
      with type ('a, 'e) f := ('a, 'e) Ret.t
      with type ('a, 'e) ret := ('a, 'e) Ret.t)
  : Sigs2.SEQMON2TRAVERSORS
      with type ('a, 'e) mon := ('a, 'e) Ret.t
      with type ('a, 'e) callermon := ('a, 'e) Alt.t
      with type ('a, 'e) t := ('a, 'e) t
end

module Make2
  (Mon: Sigs2.MONAD2)
: S2
   with type ('a, 'e) mon := ('a, 'e) Mon.t


(**/**)
(* For tests we need the types to be injective *)

module type INJ_S2 = sig
  type (!'a, 'e) mon
  type ('a, 'e) t = unit -> (('a, 'e) node, 'e) mon
  and ('a, 'e) node =
    | Nil
    | Cons of 'a * ('a, 'e) t

  include Sigs2.SEQMON2ALL
    with type ('a, 'e) mon := ('a, 'e) mon
    with type ('a, 'e) t := ('a, 'e) t

  module M
  : Sigs2.SEQMON2TRANSFORMERS
    with type ('a, 'e) mon := ('a, 'e) mon
    with type ('a, 'e) callermon := ('a, 'e) mon
    with type ('a, 'e) t := ('a, 'e) t

  module Make
    (Alt : Sigs2.INJ_MONAD2)
    (Glue : Sigs2.GLUE2
      with type ('a, 'e) x := ('a, 'e) Alt.t
      with type ('a, 'e) f := ('a, 'e) mon
      with type ('a, 'e) ret := ('a, 'e) mon)
  : Sigs2.SEQMON2TRANSFORMERS
      with type ('a, 'e) mon := ('a, 'e) mon
      with type ('a, 'e) callermon := ('a, 'e) Alt.t
      with type ('a, 'e) t := ('a, 'e) t

  module MakeTraversors
    (Alt : Sigs2.INJ_MONAD2)
    (Ret : Sigs2.INJ_MONAD2)
    (GlueAlt : Sigs2.GLUE2
      with type ('a, 'e) x := ('a, 'e) Alt.t
      with type ('a, 'e) f := ('a, 'e) Ret.t
      with type ('a, 'e) ret := ('a, 'e) Ret.t)
    (GlueMon : Sigs2.GLUE2
      with type ('a, 'e) x := ('a, 'e) mon
      with type ('a, 'e) f := ('a, 'e) Ret.t
      with type ('a, 'e) ret := ('a, 'e) Ret.t)
  : Sigs2.SEQMON2TRAVERSORS
      with type ('a, 'e) mon := ('a, 'e) Ret.t
      with type ('a, 'e) callermon := ('a, 'e) Alt.t
      with type ('a, 'e) t := ('a, 'e) t
end

module Make2Injective
  (Mon: Sigs2.INJ_MONAD2)
: INJ_S2
   with type ('a, 'e) mon := ('a, 'e) Mon.t
