type (!'a, 'e) mon = 'a
type (!'a, 'e) t = 'a
val return : 'a -> ('a, 'e) t
val bind : ('a, 'e) t -> ('a -> ('b, 'e) t) -> ('b, 'e) t
