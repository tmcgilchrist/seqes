(**************************************************************************)
(*                                                                        *)
(*                                 OCaml                                  *)
(*                                                                        *)
(*                 Raphaël Proust                                         *)
(*                                                                        *)
(*   Copyright 2022 Nomadic Labs                                          *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

(* Tests Monadic.Make2 against Stdlib *)

(* We test Monadic.Make2 by feeding it the Identity Monad: the resulting module should be
   indistinguishable from [Stdlib.Seq] (modulo the type being new) *)
module SeqM = Seqes.Monadic.Make2Injective(Seqes.Identity2)
type e = | (* a concrete type for the second type parameter *)

module TestsSeqM = Helpers.MakeTestSuites(struct
  include SeqM
  type 'a t = ('a, e) SeqM.t
end)

module TestsSeqMTransformers = Helpers.MakeTestSuites(struct
  include SeqM
  include SeqM.M (* overwrite the transformers *)
  type 'a t = ('a, e) SeqM.t
end)

module SeqMTraversors = SeqM.MakeTraversors
  (Seqes.Identity2)
  (Seqes.Identity2)
  (Seqes.Identity2)
  (Seqes.Identity2)

module TestsSeqMTraversors = Helpers.MakeTestSuites(struct
  include SeqM
  include SeqMTraversors (* overwrite the traversors *)
  type 'a t = ('a, e) SeqM.t
end)


let () =
  Alcotest.run
  "Seqes.Monadic.Make2"
  [
    ("Van2", TestsSeqM.all);
    ("Van2.M", TestsSeqMTransformers.transformers);
    ("Van2.Trav", TestsSeqMTraversors.traversors);
  ]
