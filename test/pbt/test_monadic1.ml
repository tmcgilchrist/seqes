(**************************************************************************)
(*                                                                        *)
(*                                 OCaml                                  *)
(*                                                                        *)
(*                 Raphaël Proust                                         *)
(*                                                                        *)
(*   Copyright 2022 Nomadic Labs                                          *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

(* Tests Monadic.Make against Stdlib *)

(* We test Monadic.Make by feeding it the Identity Monad: the resulting module should be
   indistinguishable from [Stdlib.Seq] (modulo the type being new) *)
module SeqM = Seqes.Monadic.Make1Injective(Seqes.Identity1)

module TestsSeqM = Helpers.MakeTestSuites(SeqM)

module TestsSeqMTransformers = Helpers.MakeTestSuites(struct
  include SeqM
  include SeqM.M (* overwrite the transformers *)
end)

module SeqMTraversors = SeqM.MakeTraversors
  (Seqes.Identity1)
  (Seqes.Identity1)
  (Seqes.Identity1)
  (Seqes.Identity1)

module TestsSeqMTraversors = Helpers.MakeTestSuites(struct
  include SeqM
  include SeqMTraversors (* overwrite the traversors *)
end)

module SeqMTraversors2 = SeqM.MakeTraversors2
  (Seqes.Identity2)
  (Seqes.Identity2)
  (Seqes.Identity2)
  (Seqes.Identity2)

module TestsSeqMTraversors2 = Helpers.MakeTestSuites(struct
  include SeqM
  include SeqMTraversors2 (* overwrite the traversors *)
end)


let () =
  Alcotest.run
  "Seqes.Monadic.Make1"
  [
    ("Van", TestsSeqM.all);
    ("Van.M", TestsSeqMTransformers.transformers);
    ("Van.Trav", TestsSeqMTraversors.traversors);
    ("Van.Trav2", TestsSeqMTraversors2.traversors);
  ]
