Uninterrupted run
  $ crammero 100000
  mod3: 0 6 12 18 24 30 36 42 48 54 60 66 72 78 84 90 96 DONE
  negative: -1 -3 -5 -7 -9 -11 -13 -15 -17 -19 -21 -23 -25 -27 -29 -31 -33 -35 -37 -39 DONE
  fibonnaci: 1 1 1 1 1 1 1 1 1 2 2 2 2 2 2 2 2 3 3 3 3 3 3 3 5 5 5 5 5 5 8 8 8 8 8 13 13 13 13 21 21 21 34 34 55 DONE

A short run
  $ crammero 20
  mod3: 0 6 12 18 INTERRUPTED
  negative: -1 -3 -5 -7 -9 -11 -13 -15 -17 -19 INTERRUPTED
  fibonnaci: 1 1 1 1 1 1 1 1 1 2 2 2 2 2 2 2 2 3 3 3 3 INTERRUPTED

A shortest run
  $ crammero 0
  mod3: 0 INTERRUPTED
  negative: INTERRUPTED
  fibonnaci: 1 INTERRUPTED

A no run
  $ crammero -1
  mod3: INTERRUPTED
  negative: INTERRUPTED
  fibonnaci: INTERRUPTED
