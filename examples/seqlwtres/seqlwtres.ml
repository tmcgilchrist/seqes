(**************************************************************************)
(*                                                                        *)
(*                                 OCaml                                  *)
(*                                                                        *)
(*                 Raphaël Proust                                         *)
(*                                                                        *)
(*   Copyright 2022 Nomadic Labs                                          *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

(* [SeqRS] is a combined result-Lwt seq-like module. It exposes all the
   functions from [Stdlib.Seq] but for sequences which may take some time to
   resolve (Lwt) and may even fail to resolve (result). *)
module SeqRS
: sig
  include Seqes.Sigs.SEQMON2ALL
    with type ('a, 'e) mon := ('a, 'e) result Lwt.t

  (* The submodules [R], [S] and [RS] expose the Seq functions specialised to
     the different sub-monads of result-Lwt. E.g.,
     {[
       map : ('a -> 'b) -> ('a, 'e) t -> ('b, 'e) t
       R.map : ('a -> ('b, 'e) result) -> ('a, 'e) t -> ('b, 'e) t
       S.map : ('a -> 'b Lwt.t) -> ('a, 'e) t -> ('b, 'e) t
       RS.map : ('a -> ('b, 'e) result Lwt.t) -> ('a, 'e) t -> ('b, 'e) t
     ]} *)
  module R :
    Seqes.Sigs.SEQMON2TRANSFORMERS
      with type ('a, 'e) mon := ('a, 'e) result Lwt.t
      with type ('a, 'e) callermon := ('a, 'e) result
      with type ('a, 'e) t := ('a, 'e) t
  module S :
    Seqes.Sigs.SEQMON2TRANSFORMERS
      with type ('a, 'e) mon := ('a, 'e) result Lwt.t
      with type ('a, 'e) callermon := 'a Lwt.t
      with type ('a, 'e) t := ('a, 'e) t
  module RS :
    Seqes.Sigs.SEQMON2TRANSFORMERS
      with type ('a, 'e) mon := ('a, 'e) result Lwt.t
      with type ('a, 'e) callermon := ('a, 'e) result Lwt.t
      with type ('a, 'e) t := ('a, 'e) t
end
= struct
  include Seqes.Monadic.Make2(struct
    type ('a, 'e) t = ('a, 'e) result Lwt.t
    let return = Lwt_result.return
    let bind = Lwt_result.bind
  end)
  module R = Make
    (struct
      type ('a, 'e) t = ('a, 'e) result
      let return x = Ok x
      let bind = Result.bind
    end)
    (struct
      let bind x f = match x with
        | Ok v -> f v
        | Error _ as err -> Lwt.return err
    end)
  module S = Make
    (struct
      type ('a, 'e) t = 'a Lwt.t
      let return = Lwt.return
      let bind = Lwt.bind
    end)
    (struct
      let bind x f = Lwt.bind x f
    end)
  module RS = M
end

let () =
  let r =
    Lwt_main.run begin
      List.to_seq ["a";"bb";"ccc";"dd";"e";"";"f";"gg"]
      |> SeqRS.of_seq
      |> SeqRS.R.mapi
          (fun i x ->
            if x = "" then Error i else Ok (x ^ x ^ x))
      |> SeqRS.S.mapi
          (fun i x ->
            let open Lwt.Syntax in
            let* () = Lwt_unix.sleep (float_of_int i /. 10.) in
            Lwt.return (x ^ x ^ x))
      |> SeqRS.RS.iter
          (fun s ->
            if String.length s > 100 then
              Lwt.return_error 999
            else
              Lwt_result.ok (Lwt_io.print (s ^ "\n")))
    end
  in
  match r with
  | Error 5 -> ()
  | _ -> assert false
