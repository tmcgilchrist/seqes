(**************************************************************************)
(*                                                                        *)
(*                                 OCaml                                  *)
(*                                                                        *)
(*                 Raphaël Proust                                         *)
(*                                                                        *)
(*   Copyright 2022 Nomadic Labs                                          *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

(* Just proving that the interface is compatible with vanilla Seq *)

module SeqSeq
: Seqes.Sigs.SEQMON1ALL
    with type 'a t := 'a Stdlib.Seq.t
    with type 'a mon := 'a
= struct
  include Stdlib.Seq
  let of_seq = Fun.id
end

let () =
  let open SeqSeq in
  iter print_int (take 10 (ints 0))
