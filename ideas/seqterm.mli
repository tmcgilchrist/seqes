(**************************************************************************)
(*                                                                        *)
(*                                 OCaml                                  *)
(*                                                                        *)
(*                 Simon Cruanes                                          *)
(*                                                                        *)
(*   Copyright 2017 Institut National de Recherche en Informatique et     *)
(*     en Automatique.                                                    *)
(*                                                                        *)
(*                 Raphaël Proust                                         *)
(*                                                                        *)
(*   Copyright 2022 Nomadic Labs                                          *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

(** Stdlib functors with terminators.

    The sequences of this module are similar to those of the Stdlib, but they
    also have a last-element. This last element is carried by the [Nil]
    constructor.

    Use-cases:

      - [(_, uninhabited) Seqterm.t] are infinite sequences
      - [('a, 'a) Seqterm.t] are non-empty sequences
      - [(s, r) Seqterm.t] for computations where the intermediate states [s]
        are of a different type than the result [r]

  *)

type ('a, 't) t = unit -> ('a, 't) node

and (+'a, +'t) node =
  | Nil of 't
  | Cons of 'a * ('a, 't) t

val is_terminated : ('a,'t) t -> 't option
val terminated : 't -> ('a,'t) t
val uncons : ('a,'t) t -> (('a * ('a,'t) t), 't) Either.t
val length : ('a,'t) t -> (int -> 't -> 'b) -> 'b
val iter : ('a -> unit) -> ('a,'t) t -> 't
val fold_left : ('a -> 'b -> 'a) -> 'a -> ('b,'t) t -> ('a -> 't -> 'c) -> 'c
val iteri : (int -> 'a -> unit) -> ('a,'t) t -> 't
val fold_lefti : ('b -> int -> 'a -> 'b) -> 'b -> ('a,'t) t -> ('b -> 't -> 'c) -> 'c
val for_all : ('a -> bool) -> ('a,'t) t -> ('t -> bool) -> bool
val exists : ('a -> bool) -> ('a,'t) t -> ('t -> bool) -> bool
val find : ('a -> bool) -> ('a,'t) t -> ('a, 't) Either.t
val find_map : ('a -> 'b option) -> ('a,'t) t -> ('b, 't) Either.t
val equal : ('a -> 'b -> bool) -> ('ta -> 'tb -> bool) -> ('a,'ta) t -> ('b,'tb) t -> bool
val compare : ('a -> 'b -> int) -> ('ta -> 'tb -> int) -> ('a,'ta) t -> ('b,'tb) t -> int
val cons : 'a -> ('a,'t) t -> ('a,'t) t
val init : int -> (int -> 'a) -> 't -> ('a,'t) t
val unfold : ('b -> (('a * 'b), 't) Either.t) -> 'b -> ('a,'t) t
val map : ('a -> 'b) -> ('a,'t) t -> ('b,'t) t
val mapt : ('t -> 'u) -> ('a,'t) t -> ('a,'u) t
val mapi : (int -> 'a -> 'b) -> ('a,'t) t -> ('b,'t) t
val filter : ('a -> bool) -> ('a,'t) t -> ('a,'t) t
val filter_map : ('a -> 'b option) -> ('a,'t) t -> ('b,'t) t
val scan : ('b -> 'a -> 'b) -> 'b -> ('a,'t) t -> ('b,'t) t
val take : int -> ('a,'t) t -> ('a,'t option) t
val drop : int -> ('a,'t) t -> ('a,'t) t
val take_while : ('a -> bool) -> ('a,'t) t -> ('a,'t option) t
val drop_while : ('a -> bool) -> ('a,'t) t -> ('a,'t) t
val group : ('a -> 'a -> bool) -> ('a,'t) t -> ('a Seq.t,'t) t
val unzip : ('a * 'b, 't) t -> ('a, 't) t * ('b, 't) t
val split : ('a * 'b, 't) t -> ('a, 't) t * ('b, 't) t
val partition : ('a -> bool) -> ('a, 't) t -> ('a, 't) t * ('a, 't) t
val partition_map : ('a -> ('b, 'c) Either.t) -> ('a, 't) t -> ('b, 't) t * ('c, 't) t
val memoize : ('a,'t) t -> ('a,'t) t
val once : ('a,'t) t -> ('a,'t) t
val append : ('a,'t) t -> ('t -> ('a,'t) t) -> ('a,'t) t
val of_dispenser : (unit -> ('a, 't) Either.t) -> ('a,'t) t
val to_dispenser : ('a,'t) t -> (unit -> ('a, 't) Either.t)

type uninhabited = |

val repeat : 'a -> ('a, uninhabited) t
val cycle : ('a, 'a) t -> ('a, uninhabited) t
val forever : (unit -> 'a) -> ('a, uninhabited) t
val iterate : ('a -> 'a) -> 'a -> ('a, uninhabited) t
val flat_iterate : ('t -> ('a, 't) t) -> 't -> ('a, uninhabited) t
val ints : int -> (int, uninhabited) t

val to_seq : ('a, 't) t -> ('t -> 'a option) -> 'a Seq.t
val of_seq : 'a Seq.t -> 't -> ('a, 't) t
